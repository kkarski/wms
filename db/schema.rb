# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160707103817) do

  create_table "item_locations", force: :cascade do |t|
    t.integer  "item_id"
    t.integer  "warehouse_id"
    t.integer  "quantity"
    t.string   "state"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "item_locations", ["item_id", "warehouse_id"], name: "index_item_locations_on_item_id_and_warehouse_id"

  create_table "items", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "permissions", force: :cascade do |t|
    t.string   "code"
    t.string   "subject_class"
    t.string   "action"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "role_permissions", force: :cascade do |t|
    t.integer  "role_id"
    t.integer  "permission_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "role_permissions", ["permission_id"], name: "index_role_permissions_on_permission_id"
  add_index "role_permissions", ["role_id"], name: "index_role_permissions_on_role_id"

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.integer  "role_id"
    t.string   "email"
    t.string   "crypted_password"
    t.string   "persistence_token"
    t.string   "single_access_token"
    t.string   "perishable_token"
    t.integer  "login_count",         default: 0,     null: false
    t.integer  "failed_login_count",  default: 0,     null: false
    t.datetime "last_request_at"
    t.datetime "current_login_at"
    t.datetime "last_login_at"
    t.string   "current_login_ip"
    t.string   "last_login_ip"
    t.boolean  "active",              default: false
    t.boolean  "approved",            default: false
    t.boolean  "confirmed",           default: false
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "users", ["active"], name: "index_users_on_active"
  add_index "users", ["approved"], name: "index_users_on_approved"
  add_index "users", ["confirmed"], name: "index_users_on_confirmed"
  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["role_id"], name: "index_users_on_role_id"

  create_table "warehouses", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.boolean  "main"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "warehouses", ["main"], name: "index_warehouses_on_main"

end
