class CreateRolePermissions < ActiveRecord::Migration
  def change
    create_table :role_permissions do |t|
      t.belongs_to :role, index: true, foreign_key: true
      t.belongs_to :permission, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
