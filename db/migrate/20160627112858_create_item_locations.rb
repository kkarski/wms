class CreateItemLocations < ActiveRecord::Migration
  def change
    create_table :item_locations do |t|
      t.integer :item_id
      t.integer :warehouse_id
      t.integer :quantity
      t.string :state

      t.timestamps null: false
    end

    add_index :item_locations, [:item_id, :warehouse_id]
  end
end
