class CreateWarehouses < ActiveRecord::Migration
  def change
    create_table :warehouses do |t|
      t.string :name
      t.string :code
      t.boolean :main

      t.timestamps null: false
    end

    add_index :warehouses, :main
  end
end
