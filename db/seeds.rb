# ruby encoding: utf-8

permission_list = [
    %w[admin all manage],
    %w[user.manage User manage],
    %w[user.index User index],
    %w[user.new User new],
    %w[user.show User show],
    %w[user.edit User edit],
    %w[user.destroy User destroy],
    %w[warehouse.manage Warehouse manage],
    %w[warehouse.index Warehouse index],
    %w[warehouse.new Warehouse new],
    %w[warehouse.show Warehouse show],
    %w[warehouse.edit Warehouse edit],
    %w[warehouse.destroy Warehouse destroy],
    %w[item.manage Item manage],
    %w[item.index Item index],
    %w[item.new Item new],
    %w[item.show Item show],
    %w[item.edit Item edit],
    %w[item.destroy Item destroy],
    %w[item_location.manage ItemLocation manage],
    %w[item_location.index ItemLocation index],
    %w[item_location.new ItemLocation new],
    %w[item_location.show ItemLocation show],
    %w[item_location.edit ItemLocation edit],
    %w[item_location.destroy ItemLocation destroy],
]

permissions = []

permission_list.each do |code, subject_class, action|
  permissions.push(Permission.create(code: code, subject_class: subject_class, action: action))
end

role_list = [
    %w[admin admin]
]

roles = []

role_list.each do |name, code|
  roles.push(Role.create(name: name, code: code))
end

admin_permission = permissions.first
admin_role = roles.first

RolePermission.create(role_id: admin_role.id, permission_id: admin_permission.id)

User.create!({
  role_id: admin_role.id,
  email: 'admin@localhost.dev',
  password: 'admin',
  password_confirmation: 'admin',
  active: true,
  approved: true,
  confirmed: true
})
