class User < ActiveRecord::Base
  belongs_to :role

  delegate :name, :to => :role, :prefix => true, :allow_nil => false

  acts_as_authentic
end
