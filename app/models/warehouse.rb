class Warehouse < ActiveRecord::Base
  has_many :item_locations, inverse_of: :warehouse, dependent: :delete_all
  has_many :items, through: :item_locations

  before_save :ensure_singular_main

  def presentation
    self.name + ' (' + self.code + ')'
  end

  protected
    def ensure_singular_main
      self.class.where('id <> ? and main = ?', self.id, true).update_all(main: false)
    end
end
