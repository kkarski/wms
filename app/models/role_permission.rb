class RolePermission < ActiveRecord::Base
  belongs_to :role, inverse_of: :role_permissions
  belongs_to :permission, inverse_of: :role_permissions
end
