class Permission < ActiveRecord::Base
  has_many :role_permissions, inverse_of: :permission
  has_many :roles, through: :role_permissions
end
