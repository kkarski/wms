class Item < ActiveRecord::Base
  has_many :item_locations, inverse_of: :item, dependent: :delete_all
  has_many :warehouses, through: :item_locations

  def quantities
    data = {}

    self.warehouses.each do |warehouse|
      key = warehouse.presentation
      location = {}

      ItemLocation.available_states.each do |state|
        location[state] = self.item_locations.in_state(warehouse, state).sum(:quantity)
      end

      data[key] = location
    end

    data
  end
end
