class ItemLocation < ActiveRecord::Base
  IN_STOCK = 'in_stock'.freeze
  INBOUND = 'inbound'.freeze
  OUTBOUND = 'outbound'.freeze
  RESERVED = 'reserved'.freeze

  belongs_to :item, inverse_of: :item_locations
  belongs_to :warehouse, inverse_of: :item_locations

  scope :in_state, ->(warehouse, state) { where(warehouse: warehouse, state: state) }

  delegate :name, :to => :item, :prefix => true, :allow_nil => false

  def self.available_states
    [ IN_STOCK, INBOUND, OUTBOUND, RESERVED ]
  end
end
