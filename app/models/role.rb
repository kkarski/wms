class Role < ActiveRecord::Base
  has_many :users, inverse_of: :role
  has_many :role_permissions, inverse_of: :role
  has_many :permissions, through: :role_permissions
end
