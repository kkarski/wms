(($) ->
  $ ->
    $('a[href*="#"]:not([href="#"])').click ->
      samePath = location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
      sameHost = location.hostname == this.hostname

      if samePath and sameHost
        target = if $(this.hash).length then $(this.hash) else $('[name=' + this.hash.slice(1) +']')

        if target.length
          $('html, body').animate
            scrollTop: target.offset().top
            500

          return false

    $('.row-linkable').dblclick ->
      window.location.href = $(this).data('url')

    $(document).ready ->
      $('.alert').alert()
) jQuery
