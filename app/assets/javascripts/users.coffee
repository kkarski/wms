$ ->
  $(document).ready ->
    $('#user-status-modal').on 'hidden.bs.modal', ->
      $('#user-status-actions').html('')
      window.location.reload(true)
