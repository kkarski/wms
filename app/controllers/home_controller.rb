class HomeController < ApplicationController
  def index
    unless current_user
      redirect_to sign_in_path
    end
  end
end
