class ItemLocationsController < ApplicationController
  load_and_authorize_resource :only => [:index, :show, :new, :edit, :destroy]

  def index
  end

  def show
  end

  def new
    @item = Item.find(params[:item_id])
    @item_location.item = @item
  end

  def create
    @item_location = ItemLocation.new(create_params)

    @item_location.save

    redirect_to item_item_location_url :id => @item_location.id
  end

  private
    def create_params
      params.require(:item_location).permit(:item_id, :warehouse_id, :in_stock, :quantity, :state)
    end
end
