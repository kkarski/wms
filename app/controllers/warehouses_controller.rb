class WarehousesController < ApplicationController
  load_and_authorize_resource :only => [:index, :show, :new, :edit]

  def index
  end

  def show
  end

  def new
  end

  def edit
  end

  def create
    @warehouse = Warehouse.new(resource_params)

    if @warehouse.save
      flash[:success] = t 'flashes.save.success'

      redirect_to @warehouse
    else
      flash[:error] = t 'flashes.save.error'

      render 'new'
    end
  end

  def update
    @warehouse = Warehouse.find(params[:id])

    if @warehouse.update_attributes(resource_params)
      flash[:success] = t 'flashes.update.success'

      redirect_to @warehouse
    else
      flash[:error] = t 'flashes.update.error'

      render 'edit'
    end
  end

  private
    def resource_params
      params.require(:warehouse).permit(:name, :code, :main)
    end
end
