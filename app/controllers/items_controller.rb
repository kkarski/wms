class ItemsController < ApplicationController
  load_and_authorize_resource :only => [:index, :show, :new, :edit, :destroy]

  def index
  end

  def show
  end

  def new
  end

  def edit
  end

  def create
    @item = Item.new(resource_params)

    if @item.save
      flash[:success] = t 'flashes.save.success'

      redirect_to @item
    else
      flash[:error] = t 'flashes.save.error'

      render 'new'
    end
  end

  def update
    @item = Item.find(params[:id])

    if @item.update_attributes(resource_params)
      flash[:success] = t 'flashes.update.success'

      redirect_to @item
    else
      flash[:error] = t 'flashes.update.error'

      render 'edit'
    end
  end

  private
  def resource_params
    params.require(:item).permit(:name, :description)
  end
end
