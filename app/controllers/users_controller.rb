class UsersController < ApplicationController
  load_and_authorize_resource :only => [:index, :show, :new, :edit, :update, :destroy]

  def index
  end

  def show
  end

  def new
  end

  def edit
  end

  def create
    @user = User.new(create_params)

    if @user.save
      flash[:success] = t 'flashes.save.success'

      redirect_to root_path
    else
      flash[:error] = t 'flashes.save.error'

      render :new
    end
  end

  def update
    if @user.update_attributes(update_params)
      flash[:success] = t 'flashes.update.success'

      redirect_to show
    else
      flash[:error] = t 'flashes.update.error'

      render :show
    end
  end

  def statuses
    @user = User.find(params[:user_id])
  end

  def update_status
    @user = User.find(params[:user_id])

    if @user.update_attributes(update_params)
      flash[:success] = t 'flashes.update.success'
    else
      flash[:error] = t 'flashes.update.error'
    end
  end

  private
    def create_params
      params.require(:user).permit(:email, :password, :password_confirmation)
    end

    def update_params
      params.require(:user).permit(:active, :approved, :confirmed)
    end
end
