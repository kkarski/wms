module ActionHelper
  def action_delete(path)
    link_to path, :method => 'delete', :class => 'text-danger', :data => {
        :confirm => (t 'actions.confirm_delete'),
        :commit => (t 'actions.commit'),
        :cancel => (t 'actions.cancel'),
        :'original-title' => (t 'actions.confirm_delete_title')
    } do
      fa_icon 'trash'
    end
  end

  def action_edit(path)
    link_to path, :class => 'text-primary' do
      fa_icon 'wrench'
    end
  end

  def action_show(path)
    link_to path, :class => 'text-primary' do
      fa_icon 'eye'
    end
  end

  def action_update_user_status(path)
    link_to path, :remote => true, :class => 'user-status-update text-primary' do
      fa_icon 'user'
    end
  end
end
