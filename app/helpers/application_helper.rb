module ApplicationHelper
  def site_name
    'Warehouse Management System'
  end

  def meta_author
    'Firebrand Software'
  end

  def meta_description
    'Application for managing various warehouse-related tasks'
  end

  def meta_keywords
    'warehouse,management,system,application,tasks'
  end

  def full_title(page_title)
    if page_title.empty?
      site_name
    else
      page_title + ' | ' + site_name
    end
  end

  def bootstrap_class_for(flash_type)
    case flash_type
      when 'success'
        'alert-success'
      when 'error'
        'alert-danger'
      when 'info'
        'alert-warning'
      else
        'alert-info'
    end
  end

  def bool_icon(bool)
    (fa_icon 'check', class: 'text-success') if bool
  end
end
