module UsersHelper
  def toggle_user_status(user, status)
    link_to user_update_user_status_path(user, "user[#{status}]" => !user.read_attribute(status)), :method => 'put', :remote => true, :class => 'update-user-status-action' do
      button_title(user.read_attribute(status), status)
    end
  end

  private
    def button_title(bool, status)
      if bool
        icon = fa_icon 'times', class: 'text-danger'
        text = t "actions.user.#{status}.f"
      else
        icon = fa_icon 'check', class: 'text-success'
        text = t "actions.user.#{status}.t"
      end

      [icon, text].join('&nbsp;').html_safe
    end
end
