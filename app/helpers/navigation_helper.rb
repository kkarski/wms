module NavigationHelper
  def navigation_data
    {
        path_translation_key('users') => users_path,
        path_translation_key('warehouses') => warehouses_path,
        path_translation_key('items') => items_path
    }
  end

  def locale_switcher
    links = []

    I18n.config.available_locales.each do |locale|
      link = link_to locale: locale do
        generate_image(locale)
      end

      links.push(link)
    end

    links.join('&nbsp;/&nbsp;').html_safe
  end

  private
    def path_translation_key(name)
      name + '.title.index'
    end

    def current_controller?(path)
      test_path = Rails.application.routes.recognize_path(path)

      test_path[:controller] == params[:controller]
    end

    def current_locale?(locale)
      current_locale = params[:locale] || I18n.config.default_locale

      locale.to_s == current_locale.to_s
    end

    def locale_img_class(locale)
      current_locale?(locale) ? 'active' : 'inactive'
    end

    def generate_image(locale)
      img_file = 'flags/' + locale.to_s + '.png'
      alt = locale.to_s.upcase

      image_tag(img_file, :alt => alt, :width => 24, :height => 24, :class => locale_img_class(locale))
    end
end
