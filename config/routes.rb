Rails.application.routes.draw do
  scope '(:locale)', locale: /en|pl/ do
    root 'home#index'

    get '/sign_in', to: 'user_sessions#new', as: :sign_in
    post '/sign_in', to: 'user_sessions#create'
    delete '/sign_out', to: 'user_sessions#destroy', as: :sign_out

    resources :warehouses
    resources :users do
      get '/user_status_info', to: 'users#statuses', as: :get_user_status
      put '/user_status', to: 'users#update_status', as: :update_user_status
    end
    resources :items do
      resources :item_locations
    end
  end
end
